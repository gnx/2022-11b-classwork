package game;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Programa {
	public static void main(String[] args) {
		Hero h = new Hero(1, 2);
		Karta k = new Karta(8, 5, h);
		GameFrame f = new GameFrame(k);
		f.setSize(500, 500);
		f.setVisible(true);
		
		File file = new File("game/input-file.txt");
		try {
			Scanner scan = new Scanner(file);
			while(scan.hasNextLine()) {
				double s = scan.nextDouble() + 6;
				System.out.println(s);
			}
			scan.close();
		} catch (FileNotFoundException e) {
			System.out.println("Няма такъв входен файл");
		}
		
		File fileo = new File("game/output-file.txt");
		try {
			FileWriter fw = new FileWriter(fileo);
			fw.write("ред едно");
			fw.close();
		} catch (IOException e) {
			System.out.println("Няма такъв изходен файл");
		}
	}
}