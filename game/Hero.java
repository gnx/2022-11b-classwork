package game;
public class Hero {
	int posx;
	int posy;
	
	Hero(int x, int y) {
		posx = x;
		posy = y;
	}
	
	void move(char c) {
		switch(c) {
			case 'w': posy--; break;
			case 's': posy++; break;
			case 'a': posx--; break;
			case 'd': posx++; break;
		}
	}
}
