package serialization;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class Programa {
	public static void main(String[] args) {
		Hrana h = new Hrana();
		h.ostavashtiDni = 20;
		h.kolichestvo = 3;
		
		// сериализация
		try {
			FileOutputStream fos = new FileOutputStream("hrana.bin");
			ObjectOutputStream oos = new ObjectOutputStream(fos);
			oos.writeObject(h);
			oos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// десериализация
		try {
			FileInputStream fis = new FileInputStream("hrana.bin");
			ObjectInputStream ois = new ObjectInputStream(fis);
			Hrana h2 = (Hrana) ois.readObject();
			
			System.out.println(h2.ostavashtiDni);
		} catch (IOException | ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
}
