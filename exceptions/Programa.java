package exceptions;

public class Programa {
	public static void main(String[] args) {
		Hero h;
		try {
			h = new Hero();
		} catch (Exception e) {
			System.out.println("Въведени са невалидни стойности за координати на героя.");
			System.out.println("Трябва да бъдат положителни числа.");
			System.out.println("Създава се герой с координатите по подразбиране - (0; 0).");

			h = new Hero(0, 0);
		}
		System.out.println(h);
	}
}
