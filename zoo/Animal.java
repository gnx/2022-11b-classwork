package zoo;
public class Animal {
	protected int legs;
	String color;
	public String name;
	double weight;
	Animal mother;
	Animal father;
	
	Animal() {}
	
	Animal(String name) {
		this.name = name;
	}
	
	Animal(String name, String color) {
		this(name);
		this.color = color;
	}
	
	Animal(String name, String color, double weight) {
		this(name, color);
		this.weight = weight;
	}
	
	void print() {
		System.out.println("Аз съм " + name);
	}
	
	void print(String greeting) {
		System.out.println(greeting);
	}
	
	void print(int age) {
		print();
		System.out.println("Аз съм на " + age + " години");
	}
	
	void printParents() {
		System.out.println("Името ми е: " + name);
		System.out.println("Името на майка ми е: " + mother.name);
		System.out.println("Името на баща ми е: " + father.name);
	}
	
	void printMaleAncestors() {
		Animal q = this;
		while(q.father != null) {
			System.out.println("Името на баща ми е: " + q.father.name);
			q = q.father;
		}
	}
	
	void printFemaleAncestors() {
		Animal q = this;
		while(q.mother != null) {
			System.out.println("Името на майка ми е: " + q.mother.name);
			q = q.mother;
		}
	}
	
//	abstract void printInfo();
	
	public String toString() {
		return "Казвам се " + name + ".\nЦветът ми е " + color + ".";
	}
}